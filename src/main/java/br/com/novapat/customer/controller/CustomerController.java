package br.com.novapat.customer.controller;


import br.com.novapat.customer.models.Customer;
import br.com.novapat.customer.models.dto.CreateCustomerRequest;
import br.com.novapat.customer.models.dto.CustomerMapper;
import br.com.novapat.customer.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/cliente")
public class CustomerController {

    @Autowired
    private CustomerService service;

    @Autowired
    private CustomerMapper mapper;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Customer create(@RequestBody CreateCustomerRequest createCustomerRequest) {
        Customer customer = mapper.toCustomer(createCustomerRequest);

        customer = service.create(customer);

        return customer;
    }

    @GetMapping("/{id}")
    public Customer getById(@PathVariable Long id) {
        System.out.println("Recebi uma requisição! " + System.currentTimeMillis());
        return service.getById(id);
    }

}
